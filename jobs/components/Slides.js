import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions } from 'react-native';
import { Button } from 'react-native-elements';

const SCREEN_WIDTH = Dimensions.get('window').width;


class Slides extends Component {
    renderLastSlide(index) {
        if (index === this.props.data.length - 1) {
            return (
                <Button
                    raised
                    title="Get Started"
                    buttonStyle={ styles.buttonStyle }
                    onPress={ this.props.onComplete }
                />
            );
        }
    }

    renderSlides() {
        return this.props.data.map(({ text, color }, index) => {
            return (
                <View
                    key={ text }
                    style={[
                        styles.slideStyle,
                        { backgroundColor: color }
                    ]}>
                    <Text
                        style={ styles.textStyle }>{ text }</Text>
                    { this.renderLastSlide(index) }
                </View>
            );
        });
    }
    render() {
        return (
            <ScrollView
                horizontal
                pagingEnabled
                style={{ flex: 1 }}>
                { this.renderSlides() }
            </ScrollView>
        );
    }
}


const styles = {
    slideStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: SCREEN_WIDTH,
    },
    textStyle: {
        fontSize: 30,
        color: '#fff'
    },
    buttonStyle: {
        backgroundColor: '#0288d1',
        marginTop: 15
    }
}

export default Slides;
