import Expo, { Notifications } from 'expo';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Alert, AsyncStorage } from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';

import registerForNotifications from './services/push_notifications';
import store from './store';
import AuthScreen from './screens/AuthScreen';
import WelcomeScreen from './screens/WelcomeScreen';
import MapScreen from './screens/MapScreen';
import DeckScreen from './screens/DeckScreen';
import ReviewScreen from './screens/ReviewScreen';
import SettingsScreen from './screens/SettingsScreen';


class App extends Component {
    async componentDidMount() {
        registerForNotifications();
        Notifications.addListener((notification) => {
            const { data: { text }, origin } = notification;

            if (origin === 'received' && text) {
                Alert.alert(
                    'New Push Notification',
                    text,
                    [{
                        text: 'OK'
                    }]
                );
            }
        });
    }

    render() {
        const MainNavigator = TabNavigator({
            welcome: { screen: WelcomeScreen },
            auth: { screen: AuthScreen },
            main: {
                screen: TabNavigator({
                    map: { screen: MapScreen },
                    deck: { screen: DeckScreen },
                    review: {
                        screen: StackNavigator({
                            review: { screen: ReviewScreen },
                            settings: { screen: SettingsScreen },
                        })
                    }
                }, {
                    lazy: true,
                    tabBarOptions: {
                        labelStyle: {
                            fontSize: 12,
                        }
                    },
                    tabBarPosition: 'bottom',
                    swipeEnabled: false,
                })
            }

        },
        /* React Navigation loads all of the pages eagerly, so we need to
         * delay AuthScreen from loading so the FB popup doesn't come up
         * unless we've clicked it. */
        {
            lazy: true,
            navigationOptions: {
                /* Hide the tab bars */
                tabBarVisible: false
            }
        });

        return (
            <Provider store={ store }>
                <MainNavigator />
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

Expo.registerRootComponent(App);
