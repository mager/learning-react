import { AsyncStorage } from 'react-native';
import { Facebook } from 'expo';

import {
    FACEBOOK_LOGIN_SUCCESS,
    FACEBOOK_LOGIN_FAIL,
} from './types';

const FB_APP_ID = '147307842029902';


/* Use async/await with redux-thunk. */
export const facebookLogin = () => async dispatch => {
    /* AsyncStorage is very similar to local storage in the browser.
    * It's just a data store. It also returns a promise because it's async.
    */
    let token = await AsyncStorage.getItem('fb_token');

    /* Dispatch an action saying FB login is successful, or else start
    * FB login process.
    */
    if (token) {
        dispatch({
            type: FACEBOOK_LOGIN_SUCCESS,
            payload: token
        });
    } else {
        doFacebookLogin(dispatch);
    }
}


const doFacebookLogin = async (dispatch) => {
    let { type, token } = await Facebook.logInWithReadPermissionsAsync(FB_APP_ID, {
        permissions: ['public_profile']
    });

    if (type === 'cancel') {
        return dispatch({ type: FACEBOOK_LOGIN_FAIL });
    }

    await AsyncStorage.setItem('fb_token', token);
    dispatch({
        type: FACEBOOK_LOGIN_SUCCESS,
        payload: token
    });

};
