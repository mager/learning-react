import React, { Component } from 'react';
import { View, Text, AsyncStorage } from 'react-native';
import { AppLoading } from 'expo';

import Slides from '../components/Slides';

const SLIDE_DATA = [
    {
        text: 'Welcome to Job App',
        color: '#03a9f4'
    },
    {
        text: 'Use this app to find a job',
        color: '#ee968a'
    },
    {
        text: 'Set your location, then swipe away',
        color: '#0647b6'
    },
];

class WelcomeScreen extends Component {
    state = {
        token: null
    };

    async componentWillMount() {
        let token = await AsyncStorage.getItem('fb_token');

        if (token) {
            this.setState({ token });
            this.props.navigation.navigate('map');
        } else {
            this.setState({ token: false });
        }
    }

    onSlidesComplete = () => {
        /* React Navigation will pass `navigation` object as props. */
        this.props.navigation.navigate('auth');
    }

    render() {
        if (this.state.token === null) {
            return <AppLoading />;
        }

        return (
            <Slides
                data={ SLIDE_DATA }
                onComplete = { this.onSlidesComplete }
            />
        );
    }
}

export default WelcomeScreen;
