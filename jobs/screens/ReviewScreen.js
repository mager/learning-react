import React, { Component } from 'react';
import { View, Text, Platform, ScrollView, Linking } from 'react-native';
import { Button, Card, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { MapView } from 'expo';


class ReviewScreen extends Component {
    /* React Navigation uses this class property for navigation options. */
    static navigationOptions = ({ navigation }) => ({
        title: 'Review Jobs',
        headerRight: (
            <Button
                title='Settings'
                onPress={ () => navigation.navigate('settings') }
                backgroundColor='rgba(0, 0, 0, 0)'
                color='rgba(0, 122, 255, 1)' />
        ),
        headerTitleStyle: {
            marginTop: Platform.OS === 'android' ? 24 : 0
        },
        tabBarVisible: true,
        tabBarIcon: ({ tintColor }) => {
            return (
                <Icon
                    name="favorite"
                    size={ 30 }
                    color={ tintColor } />
            );
        },
    });

    renderLikedJobs() {
        return this.props.likedJobs.map(job => {
            const {
                company,
                formattedRelativeTime,
                url,
                latitude,
                longitude,
                jobtitle,
                jobkey,
            } = job;
            const isAndroid = Platform.OS === 'android';
            const initialRegion = {
                longitude: longitude,
                latitude: latitude,
                longitudeDelta: 0.02,
                latitudeDelta: 0.045,
            };

            return (
                <Card
                    key={ jobkey }
                    title={ jobtitle }>
                    <View style={{ height: 200 }}>
                        <MapView
                            style={{ flex: 1 }}
                            cacheEnabled={ isAndroid }
                            scrollEnabled={ false }
                            initialRegion={ initialRegion }>
                        </MapView>
                        <View style={ styles.detailWrapper }>
                            <Text style={ styles.italics }>
                                { company }
                            </Text>
                            <Text style={ styles.italics }>
                                { formattedRelativeTime }
                            </Text>
                        </View>
                        <Button
                            title="Apply Now!"
                            backgroundColor="#939393"
                            onPress={ () => Linking.openURL(url) }
                        />
                    </View>
                </Card>
            );
        });
    }


    render() {
        return (
            <View>
                <ScrollView>
                    { this.renderLikedJobs() }
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { likedJobs } = state;

    return { likedJobs };
}

const styles = {
    detailWrapper: {
        marginTop: 10,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    italics: {
        fontStyle: 'italic'
    },
}

export default connect(mapStateToProps, null)(ReviewScreen);
