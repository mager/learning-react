import React from 'react';
import { Sparklines, SparklinesLine } from 'react-sparklines';

export default props => {
  return (
    <div style={{ height: '300px', width: '300px' }}>
      <Sparklines data={props.data}>
        <SparklinesLine
          style={{ strokeWidth: 3, stroke: props.color, fill: 'none' }}
        />
      </Sparklines>
    </div>
  );
};
