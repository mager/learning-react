import React, { Component } from 'react';
import Chart from './Chart';
import Map from './Map';

import { connect } from 'react-redux';

class WeatherList extends Component {
  componentDidMount() {
    console.log(this.props);
  }

  renderWeather() {
    return this.props.weather.length
      ? this.props.weather.map(data => {
          const name = data.city.name;
          const temps = data.list.map(weather => weather.main.temp);
          const pressures = data.list.map(weather => weather.main.pressure);
          const humidities = data.list.map(weather => weather.main.humidity);
          const { lat, lon } = data.city.coord;
          return (
            <tr key={name}>
              <td>
                <Map coords={[lon, lat]} />
              </td>
              <td>
                <Chart data={temps} color="orange" units="K" />
              </td>
              <td>
                <ul>
                  <Chart data={pressures} color="green" units="hPa" />
                </ul>
              </td>
              <td>
                <Chart data={humidities} color="black" units="%" />
              </td>
            </tr>
          );
        })
      : null;
  }

  render() {
    return (
      <section className="section">
        <table className="table is-fullwidth">
          <thead>
            <tr>
              <th>City</th>
              <th>Temperature (Kelvin)</th>
              <th>Pressure (hPa)</th>
              <th>Humidity (%)</th>
            </tr>
          </thead>
          <tbody>
            {this.renderWeather()}
          </tbody>
        </table>
      </section>
    );
  }
}

const mapStateToProps = ({ weather }) => {
  return { weather };
};

export default connect(mapStateToProps, null)(WeatherList);
