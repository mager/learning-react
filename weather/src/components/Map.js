import React, { Component } from 'react';
import ReactMapboxGl from 'react-mapbox-gl';

const Map = ReactMapboxGl({
  accessToken:
    'pk.eyJ1IjoibWFnZXIiLCJhIjoiY2lobWxvZXRpMG90ZXY1a2x4eG4wNGs1NyJ9.vEVHfV_K15rnm4_niRNHYw',
});

export default class MapComponent extends Component {
  render() {
    return (
      <Map
        // eslint-disable-next-line
        style="mapbox://styles/mapbox/streets-v9"
        containerStyle={{
          height: '200px',
          width: '200px',
        }}
        center={this.props.coords}
      />
    );
  }
}
