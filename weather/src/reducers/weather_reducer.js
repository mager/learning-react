export default function(state = [], action) {
  switch (action.type) {
    case 'FETCH_WEATHER_SUCCESS':
      return [...state, action.payload];
    case 'FETCH_WEATHER_FAILURE':
      return state;
    default:
      return [];
  }
}
