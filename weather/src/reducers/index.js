import { combineReducers } from 'redux';
import weather from './weather_reducer';

const rootReducer = combineReducers({
  weather: weather,
});

export default rootReducer;
