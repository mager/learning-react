import axios from 'axios';

export const fetchWeather = city => async dispatch => {
  const API_KEY = '5a023f7ec835d7de0baa824f83ecd734';
  const base_path = 'https://api.openweathermap.org/data/2.5/forecast';
  const params = `?q=${city},us&appId=${API_KEY}`;
  const endpoint = `${base_path}${params}`;

  try {
    let { data } = await axios.get(endpoint);

    return dispatch({
      type: 'FETCH_WEATHER_SUCCESS',
      payload: data,
    });
  } catch (e) {
    console.log(e);

    return dispatch({
      type: 'FETCH_WEATHER_FAILURE',
    });
  }
};
